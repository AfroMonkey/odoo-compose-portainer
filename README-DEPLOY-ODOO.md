# Create and deploy template for odoo on Portainer app

## Create template

### Select server
![alt text](./images/select-server.png)

### Select template menu
![alt text](./images/select-template-menu.png)

### Configure template main data
![alt text](./images/configure-template.png)

The [original docker-compose](https://gitlab.com/HomebrewSoft/homebrewsh/odoo-instances/-/blob/server/templates/13.0/docker-compose.yml) was modified with this [new docker-compose](https://gitlab.com/ereyes/odoo-compose-portainer/-/blob/master/docker-compose.odoo.yml), the main modifications were oriented to the use of environment variables

### Configure enviroments variables
ODOO_VERSION:![alt text](./images/configure-template-variable-odoo-version.png)

INSTANCE:![alt text](./images/configure-template-variable-instance.png)

VERSION:![alt text](./images/configure-template-variable-version.png)

DOMAIN:![alt text](./images/configure-template-variable-domain.png)

CONTACT:![alt text](./images/configure-template-variable-contact.png)

MAIN_PATH:![alt text](./images/configure-template-variable-path.png)

MASTER_PASSWORD:![alt text](./images/configure-template-variable-masterpassword.png)

CREATE:![alt text](./images/create-template.png)

### Variables:
- ODOO_VERSION: Odoo versions (13.0, 12.0, 11.0, 10.0, 9.0) by default:"13.0".
- INSTANCE: Main project's name, by default "manager".
- VERSION: Instance's name, by defult "instanceN".
- DOMAIN: Domain name "my.domain.net".
- CONTACT: Contact's email to support, by defult "email@gmail.com".
- MAIN_PATH: Root path where will be all data, by default "/home/ubuntu"
- MASTER_PASSWORD: Master passowrd for odoo instance, by default "12345678"

## Deploy new instance using template

Select server:![alt text](./images/select-server.png)

Select template menu:![alt text](./images/select-odoo-template.png)

Deploy template:![alt text](./images/deploy-odoo-template.png)

## Manage instances

Containers menu![alt text](./images/container-menu.png)